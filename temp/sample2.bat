@echo off 
rem Please, modify and run the 'wsetenv.bat' batch file first in order to
rem setup path to built binaries, etc.

echo Image segmentation using the FCM clustering technique...
mdsLoadDicom <../data/dicom/80.dcm |mdsSliceSegFCM -clusters 3 |mdsSliceView -coloring segmented

