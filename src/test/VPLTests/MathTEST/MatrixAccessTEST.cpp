//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/12/04                       
 * 
 * Description:
 * - Testing of the vpl::CMatrix template.
 */

#include <VPL/Math/Matrix.h>
#include <VPL/Math/Vector.h>
#include <VPL/Math/MatrixFunctions.h>

// STL
#include <iostream>
#include <ctime>

//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * Clock counter
 */
clock_t clockCounter;


//==============================================================================
/*!
 * Starts time measuring
 */
void begin()
{
    clockCounter = clock();
}

//==============================================================================
/*!
 * Stops time measuring and prints result
 */
void end()
{
    clockCounter = clock() - clockCounter;
    std::cout << "  Measured clock ticks: " << clockCounter << std::endl;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    int c;
    const int count = 100000;

    std::cout << "Testing matrix access overhead" << std::endl;
    typedef vpl::math::CMatrix<double> tTestMatrix;
    tTestMatrix m6(64,64);

    std::cout << "  Basic version" << std::endl;
    begin();
    for( c = 0; c < count; ++c )
    {
        for( int j = 0; j < m6.getNumOfRows(); ++j )
        {
            for( int i = 0; i < m6.getNumOfCols(); ++i )
            {
                m6(j, i) = 10.0;
            }
        }
    }
    end();
    keypress();

    std::cout << "  Iterator version" << std::endl;
    begin();
    for( c = 0; c < count; ++c )
    {
        for( tTestMatrix::tIterator it(m6); it; ++it )
        {
            *it = 11.0;
        }
    }
    end();
    keypress();

    std::cout << "  Fill version" << std::endl;
    begin();
    for( c = 0; c < count; ++c )
    {
        m6.fill(12.0);
    }
    end();

    return 0;
}

