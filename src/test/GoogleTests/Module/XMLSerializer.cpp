//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

// Configuration file prepared by CMake
#include <VPL/configure.h>

#if defined(VPL_XML_ENABLED)

#include "gtest/gtest.h"
#include "VPL/Module/XMLSerializer.h"
#include "VPL/Image/DensityVolume.h"
#include <VPL/Test/Utillities/arguments.h>
#include <VPL/ImageIO/DicomDirLoader.h>
#include <VPL/ImageIO/PNG.h>
#include <VPL/Test/Compare/compare2D.h>
#include <VPL/Module/BlockChannel.h>
#include "VPL/Test/Compare/compare3D.h"
#include <VPL/Test/Utillities/testingData.h>

namespace xmlSerializer
{

//! loading PNG image
void loadImage(vpl::img::CImage16& image, const std::string& path)
{
    const vpl::mod::CFileChannelPtr inChannel = vpl::mod::CFileChannelPtr(
        new vpl::mod::CFileChannel(vpl::mod::CH_IN, path));

    if (!loadPNG(image, *inChannel))
    {
        FAIL() << "Image: " << path << " is not loaded";
    }
}

#ifdef GENERATE_REFERENCE_DATA

//! Create data for XML testing
TEST(CreateData, XML)
{
    vpl::test::Arguments& testArgv = vpl::test::Arguments::get();
    std::string dirPath = ".";
    testArgv.value("dir", dirPath);

    const vpl::img::CDensityVolumePtr volume;
    vpl::img::CDicomDirLoader loader;
    loader.loadDicomDir(dirPath + "/dicom/DICOM1");
    loader.makeDensityVolume(*volume);

    vpl::img::CDicomSlice slice;
    loader.getSlice(slice, 0);

    vpl::img::CImage16 image;

    {
        vpl::mod::CFileChannel channel(vpl::mod::CH_OUT, dirPath + "/DICOM1.vol");
        vpl::mod::CXMLSerializer xml(&channel);
        xml.writeRoot(*volume);
    }

    {
        vpl::mod::CFileChannel channel(vpl::mod::CH_OUT, dirPath + "/DICOM1.dicslice");
        vpl::mod::CXMLSerializer xml(&channel);
        xml.writeRoot(slice);
    }

    {
        loadImage(image, dirPath + "/images/lena.png");

        vpl::mod::CFileChannel channel(vpl::mod::CH_OUT, dirPath + "/lena.img16");
        vpl::mod::CXMLSerializer xml(&channel);
        xml.writeRoot(image);
    }
}
#endif

//! Test fixture
class XmlSerializerTest : public ::testing::Test
{
public:
    std::string dirPath;
    void SetUp() override
    {
        dirPath = vpl::test::TestingData::getDirectoryPath();
    }
    void TearDown() override {}
};


void checkVolume(vpl::img::CDensityVolume& volume, const std::string& referencePath, const std::string& dirPath)
{
    std::string testPath = dirPath + "/temp.vol";
    //! Serialize volume
    vpl::mod::CFileChannel channel(vpl::mod::CH_OUT, testPath);
    vpl::mod::CXMLSerializer xml(&channel);
    xml.writeRoot(volume);
    channel.flush();
    channel.disconnect();

    //! Deserialize volume
    vpl::img::CDensityVolume volumeReaded;
    vpl::mod::CFileChannel channelRead(vpl::mod::CH_IN, testPath);
    if (channelRead.isConnected())
    {
        vpl::mod::CXMLSerializer xmlRead(&channelRead);
        xmlRead.readRoot(volumeReaded);
    }
    else
    {
        FAIL() << "Channel read is not opened";
    }
    channelRead.disconnect();
    //! Remove temporary file
    remove(testPath.c_str());

    //! Read reference volume for testing
    vpl::img::CDensityVolume volumeReference;
    vpl::mod::CFileChannel channelReference(vpl::mod::CH_IN, dirPath + referencePath);
    if (channelReference.isConnected())
    {

        vpl::mod::CXMLSerializer xmlReference(&channelReference, vpl::mod::Serializer::XML);
        xmlReference.readRoot(volumeReference);
    }
    else
    {
        FAIL() << "Channel reference is not opened";
    }

    //! Check data
    EXPECT_EQ(volumeReference.getMargin(), volumeReaded.getMargin());
    EXPECT_EQ(volumeReference.getXSize(), volumeReaded.getXSize());
    EXPECT_EQ(volumeReference.getYSize(), volumeReaded.getYSize());
    EXPECT_EQ(volumeReference.getXOffset(), volumeReaded.getXOffset());
    EXPECT_EQ(volumeReference.getYOffset(), volumeReaded.getYOffset());
    EXPECT_EQ(volumeReference.height(), volumeReaded.height());
    EXPECT_EQ(volumeReference.width(), volumeReaded.width());

    EXPECT_NEAR(volumeReference.getDX(), volumeReaded.getDX(),0.0001);
    EXPECT_NEAR(volumeReference.getDY(), volumeReaded.getDY(), 0.0001);
    EXPECT_NEAR(volumeReference.getDZ(), volumeReaded.getDZ(), 0.0001);

    vpl::test::Compare3D<vpl::img::tDensityPixel, vpl::img::CDensityVolume> compare;
    compare.enableAssert(true);
    compare.valuesWithReference(volumeReference, volumeReaded, volumeReference.getXSize(), volumeReference.getYSize(), volumeReference.getZSize());
}

void checkSlice(vpl::img::CSlice& slice, const std::string& referencePath, const std::string& dirPath)
{
    //! Serialize slice
    std::string testPath = dirPath + "/temp.dicslice";
    vpl::mod::CFileChannel channel(vpl::mod::CH_OUT, testPath);
    vpl::mod::CXMLSerializer xml(&channel);
    xml.writeRoot(slice);
    channel.flush();
    channel.disconnect();

    //! Deserialize slice
    vpl::img::CSlice sliceReaded;
    vpl::mod::CFileChannel channelRead(vpl::mod::CH_IN, testPath);
    if (channelRead.isConnected())
    {
        vpl::mod::CXMLSerializer xmlRead(&channelRead);
        xmlRead.readRoot(sliceReaded);
    }
    else
    {
        FAIL() << "Channel read is not opened";
    }
    channelRead.disconnect();
    //! Remove temporary file
    remove(testPath.c_str());

    //! Read reference slice for testing
    vpl::img::CSlice sliceReference;
    vpl::mod::CFileChannel channelReference(vpl::mod::CH_IN, dirPath + referencePath);
    if (channelReference.isConnected())
    {

        vpl::mod::CXMLSerializer xmlReference(&channelReference, vpl::mod::Serializer::XML);
        xmlReference.readRoot(sliceReference);
    }
    else
    {
        FAIL() << "Channel reference is not opened";
    }

    //! Check data
    EXPECT_EQ(sliceReference.getMargin(), sliceReaded.getMargin());
    EXPECT_EQ(sliceReference.getXSize(), sliceReaded.getXSize());
    EXPECT_EQ(sliceReference.getYSize(), sliceReaded.getYSize());
    EXPECT_EQ(sliceReference.getXOffset(), sliceReaded.getXOffset());
    EXPECT_EQ(sliceReference.getYOffset(), sliceReaded.getYOffset());
    EXPECT_EQ(sliceReference.height(), sliceReaded.height());
    EXPECT_EQ(sliceReference.width(), sliceReaded.width());

    EXPECT_EQ(sliceReference.getOrientation(), sliceReaded.getOrientation());
    EXPECT_NEAR(sliceReference.getPosition(), sliceReaded.getPosition(),0.0001);
    EXPECT_NEAR(sliceReference.getThickness(), sliceReaded.getThickness(), 0.0001);
    EXPECT_NEAR(sliceReference.getDX(), sliceReaded.getDX(), 0.0001);
    EXPECT_NEAR(sliceReference.getDY(), sliceReaded.getDY(), 0.0001);

    vpl::test::Compare2D<vpl::img::tDensityPixel, vpl::img::CSlice> compare;
    compare.enableAssert(true);
    compare.valuesWithReference(sliceReference, sliceReaded, sliceReference.getXSize(), sliceReference.getYSize());
}

void checkImage(vpl::img::CImage16& image, const std::string& referencePath, const std::string& dirPath)
{
    //! Serialize image
    std::string testPath = dirPath + "/templena.img16";
    vpl::mod::CFileChannel channel(vpl::mod::CH_OUT, testPath);
    vpl::mod::CXMLSerializer xml(&channel);
    xml.writeRoot(image);
    channel.flush();
    channel.disconnect();

    //! Deserialize image
    vpl::img::CImage16 imageReaded;
    vpl::mod::CFileChannel channelRead(vpl::mod::CH_IN, testPath);
    if (channelRead.isConnected())
    {
        vpl::mod::CXMLSerializer xmlRead(&channelRead);
        xmlRead.readRoot(imageReaded);
    }
    else
    {
        FAIL() << "Channel read is not opened";
    }
    channelRead.disconnect();
    //! Remove temporary file
    remove(testPath.c_str());

    //! Read reference image for testing
    vpl::img::CImage16 imageReference;
    vpl::mod::CFileChannel channelReference(vpl::mod::CH_IN, dirPath + referencePath);
    if (channelReference.isConnected())
    {

        vpl::mod::CXMLSerializer xmlReference(&channelReference, vpl::mod::Serializer::XML);
        xmlReference.readRoot(imageReference);
    }
    else
    {
        FAIL() << "Channel reference is not opened";
    }

    //! Check data
    EXPECT_EQ(imageReaded.getMargin(), imageReference.getMargin());
    EXPECT_EQ(imageReaded.getXSize(), imageReference.getXSize());
    EXPECT_EQ(imageReaded.getYSize(), imageReference.getYSize());
    EXPECT_EQ(imageReaded.getXOffset(), imageReference.getXOffset());
    EXPECT_EQ(imageReaded.getYOffset(), imageReference.getYOffset());
    EXPECT_EQ(imageReaded.height(), imageReference.height());
    EXPECT_EQ(imageReaded.width(), imageReference.width());

    vpl::test::Compare2D<vpl::img::tPixel16, vpl::img::CImage<vpl::img::tPixel16>> compare;
    compare.enableAssert(true);
    compare.valuesWithReference(imageReference, imageReaded, imageReference.getXSize(), imageReference.getYSize());
}



TEST_F(XmlSerializerTest, VolumeSlice)
{
    const vpl::img::CDensityVolumePtr volume;
    vpl::img::CDicomDirLoader loader;
    if (!loader.loadDicomDir(dirPath + "/dicom/DICOM1"))
    {
        FAIL() << "Dicoms directory not loaded.";
    }
    if (!loader.makeDensityVolume(*volume))
    {
        FAIL() << "Make density volume failed.";
    }
    const vpl::img::CDicomSlicePtr slice;
    loader.getSlice(*slice, 0);

    checkVolume(*volume, "/DICOM1.vol", dirPath);
    checkSlice(*slice, "/DICOM1.dicslice", dirPath);

}

TEST_F(XmlSerializerTest, Image)
{
    const vpl::img::CImage16Ptr image;
    loadImage(*image, dirPath + "/images/lena.png");
    if (::testing::Test::HasFatalFailure()) return;

    checkImage(*image, "/lena.img16", dirPath);
}

}
#endif
