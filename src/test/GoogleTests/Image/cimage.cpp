//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#include "gtest/gtest.h"
#include "VPL/Image/Image.h"

#include "VPL/Module/DensityCompressor.h"
#include <VPL/Image/ImageFunctions.h>
#include <VPL/Test/printDefinition.h>
#include <VPL/Base/Types.h>
#include <VPL/Test/Compare/compare2D.h>

namespace image
{
//! \brief 
class ImageDTest : public testing::Test
{
public:
    vpl::img::CDImagePtr spIm1;

    vpl::test::Compare2D<vpl::img::tDensityPixel, vpl::img::CDImage> compare;
    vpl::img::CDImage* image{};
    vpl::img::CDImagePtr image3x3;

    void SetUp() override
    {
        spIm1 = vpl::img::CDImagePtr(new vpl::img::CDImage(10, 10, 1));
        image = new vpl::img::CDImage(10, 12, 1);
        image3x3 = vpl::img::CDImagePtr(new vpl::img::CDImage(3, 3, 1));
        compare.enablePrintData(true);
        compare.setErrorMessage("Image is differ at index");
    }
    void TearDown() override
    {
        delete image;
    }

    //! \brief 
    //! \param image 
    //! \param startValue 
    //! \param step 
    //! \param isRowOrder 
    static void generateIncreasingImage(vpl::img::CDImage& image, const int startValue, const int step = 1, bool isRowOrder = true)
    {
        int value = startValue;
        for (vpl::tSize y = 0; y < image.getYSize(); y++)
        {
            for (vpl::tSize x = 0; x < image.getXSize(); x++)
            {
                if (isRowOrder)
                {
                    image(x, y) = value;
                }
                else
                {
                    image(y, x) = value;
                }
                value += step;
            }
        }
    }
};

//!
TEST_F(ImageDTest, BaseConstructor)
{
    EXPECT_EQ(10, image->getXSize()) << "Different X size";
    EXPECT_EQ(12, image->getYSize()) << "Different Y size";
    EXPECT_EQ(1, image->getMargin()) << "Different Margin size";
}

//!
TEST_F(ImageDTest, Addition)
{
    vpl::img::CDImage image2(3, 3, 1);
    generateIncreasingImage(image2, 0);
    spIm1->fillEntire(vpl::img::CDImage::tPixel(2));
    *spIm1 += image2;
    vpl::img::tDensityPixel reqData[] =
    {
        2,3,4,2,2,2,2,2,2,2,
        5,6,7,2,2,2,2,2,2,2,
        8,9,10,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2
    };
    compare.values(reqData, *spIm1, spIm1->getXSize(), spIm1->getYSize());
}

//!
TEST_F(ImageDTest, MultiplyScalar)
{
    generateIncreasingImage(*image3x3, 0);
    *image3x3 *= vpl::CScalar<int>(2);
    vpl::img::tDensityPixel reqData[] =
    {
        0,2,4,
        6,8,10,
        12,14,16
    };
    compare.values(reqData, *image3x3, 3, 3);
}

//!
TEST_F(ImageDTest, DivideScalar)
{
    generateIncreasingImage(*image3x3, 0, 3);
    *image3x3 /= vpl::CScalar<int>(2);
    vpl::img::tDensityPixel reqData[] =
    {
        0,1,3,
        4,6,7,
        9,10,12
    };
    compare.values(reqData, *image3x3, 3, 3);
}

//!
TEST_F(ImageDTest, FuncMinMaxSumMean)
{
    spIm1->fillEntire(2);
    generateIncreasingImage(image3x3->makeRef(*spIm1, 0, 0, 3, 3), 2);
    generateIncreasingImage(image3x3->makeRef(*spIm1, 5, 5, 3, 3), 0, 2);

    vpl::img::tDensityPixel reqData[] =
    {
        2,3,4,2,2,2,2,2,2,2,
        5,6,7,2,2,2,2,2,2,2,
        8,9,10,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,0,2,4,2,2,
        2,2,2,2,2,6,8,10,2,2,
        2,2,2,2,2,12,14,16,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2
    };
    compare.values(reqData, *spIm1, spIm1->getXSize(), spIm1->getYSize());
    ASSERT_DOUBLE_EQ(0, vpl::img::getMin<double>(*spIm1));
    ASSERT_DOUBLE_EQ(16, vpl::img::getMax<double>(*spIm1));
    ASSERT_DOUBLE_EQ(290, vpl::img::getSum<double>(*spIm1));
    ASSERT_DOUBLE_EQ(2.9, vpl::img::getMean<double>(*spIm1));
}

//!
TEST_F(ImageDTest, Iterator)
{
    generateIncreasingImage(*spIm1, 0);
    vpl::img::CDImage::tIterator it(*spIm1);
    ASSERT_EQ((int)it.getSize(), spIm1->getXSize() * spIm1->getYSize());

    int  value = 0;
    for (; it; ++it)
    {
        ASSERT_EQ(value, *it);
        value++;
    }
}

//!
TEST_F(ImageDTest, GettingPixel)
{
    generateIncreasingImage(*image3x3, 0);
    ASSERT_EQ(0, image3x3->at(0, 0));
    ASSERT_EQ(1, image3x3->at(1, 0));
    ASSERT_EQ(2, image3x3->at(2, 0));
    ASSERT_EQ(3, image3x3->at(0, 1));
    ASSERT_EQ(5, image3x3->at(2, 1));
    ASSERT_EQ(8, image3x3->at(2, 2));

    ASSERT_EQ(0, (*image3x3)(0, 0));
    ASSERT_EQ(1, (*image3x3)(1, 0));
    ASSERT_EQ(2, (*image3x3)(2, 0));
    ASSERT_EQ(3, (*image3x3)(0, 1));
    ASSERT_EQ(5, (*image3x3)(2, 1));
    ASSERT_EQ(8, (*image3x3)(2, 2));

}

//!
TEST_F(ImageDTest, SettingPixel)
{
    image3x3->set(0, 0, 0);
    image3x3->set(2, 2, 0);

    ASSERT_EQ(0, image3x3->at(0, 0));
    ASSERT_EQ(0, image3x3->at(2, 2));

    image3x3->set(0, 0, 15);
    image3x3->set(2, 2, 5);

    ASSERT_EQ(15, image3x3->at(0, 0));
    ASSERT_EQ(5, image3x3->at(2, 2));

}

//!
TEST_F(ImageDTest, FillEntire)
{
    vpl::img::CDImage image(15, 15, 1);
    image.fillEntire(10);
    compare.values(10, image, 15, 15);
}

//!
TEST_F(ImageDTest, ConstructorReferences)
{
    spIm1->fillEntire(vpl::img::CDImage::tPixel(2));
    vpl::img::CDImage image2(*spIm1, 5, 5, 3, 3, vpl::REFERENCE);
    for (vpl::tSize j = 0; j < image2.getYSize(); j++)
    {
        for (vpl::tSize i = 0; i < image2.getXSize(); i++)
        {
            image2(i, j) = j * image2.getYSize() + i;
        }
    }
    compare.sequence(image2, 3, 3, 0, 1);


    vpl::img::tDensityPixel test1[] =
    {
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,0,1,2,2,2,
        2,2,2,2,2,3,4,5,2,2,
        2,2,2,2,2,6,7,8,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2
    };

    compare.values(test1, *spIm1, 10, 10);
}
//!
TEST_F(ImageDTest, Resize)
{
    const unsigned int newSizeRatio = 3;
    const int xSizeNew = spIm1->getXSize() * newSizeRatio;
    const int ySizeNew = spIm1->getYSize() * newSizeRatio;
    spIm1->resize(xSizeNew, ySizeNew, 5);
    ASSERT_EQ(xSizeNew, spIm1->getXSize());
    ASSERT_EQ(ySizeNew, spIm1->getYSize());

    ASSERT_EQ(5, spIm1->getMargin());
}

//!
TEST_F(ImageDTest, ConstructorWithoutReferences)
{
    spIm1->fillEntire(vpl::img::CDImage::tPixel(2));
    vpl::img::CDImage image2(*spIm1, 5, 5, 3, 3);
    generateIncreasingImage(image2, 0, 2);
    compare.sequence(image2, 3, 3, 0, 2);

    compare.values(2, *spIm1, 10, 10);
}

//! Testing operation with margin
TEST_F(ImageDTest, Margin)
{
    vpl::img::CDImage image(3, 3, 2);
    image.fillEntire(vpl::img::CDImage::tPixel(0));
    ASSERT_EQ(2, image.getMargin());

    compare.valuesWithMargin(static_cast<vpl::img::tDensityPixel>(0), image, 3, 3, image.getMargin());


    generateIncreasingImage(image, 0, 1, false);

    vpl::img::tDensityPixel reqData[] =
    {
        0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,
        0,0,0,3,6,0,0,
        0,0,1,4,7,0,0,
        0,0,2,5,8,0,0,
        0,0,0,0,0,0,0,
        0,0,0,0,0,0,0
    };
    compare.valuesWithMargin(reqData, image, 3, 3, image.getMargin());
    image.fillMargin(1);

    vpl::img::tDensityPixel reqData2[] =
    {
        1,1,1,1,1,1,1,
        1,1,1,1,1,1,1,
        1,1,0,3,6,1,1,
        1,1,1,4,7,1,1,
        1,1,2,5,8,1,1,
        1,1,1,1,1,1,1,
        1,1,1,1,1,1,1
    };
    compare.valuesWithMargin(reqData2, image, 3, 3, image.getMargin());

    image.mirrorMargin();

    vpl::img::tDensityPixel reqData3[] =
    {
        8,5,2,5,8,5,2,
        7,4,1,4,7,4,1,
        6,3,0,3,6,3,0,
        7,4,1,4,7,4,1,
        8,5,2,5,8,5,2,
        7,4,1,4,7,4,1,
        6,3,0,3,6,3,0
    };
    compare.valuesWithMargin(reqData3, image, 3, 3, image.getMargin());

    { // shouldn't do anything (just don't crash)
        vpl::img::CDImage img1(0, 0, 1);
        img1.mirrorMargin();
        vpl::img::CDImage img2(0, 0, 8);
        img2.mirrorMargin();
    }
}

//! Testing access by smart pointer
TEST_F(ImageDTest, SmartPointer)
{
    spIm1->fillEntire(vpl::img::CDImage::tPixel(2));
    compare.values(2, *spIm1, 10, 10);
    spIm1->fillEntire(vpl::img::CDImage::tPixel(5));
    compare.values(5, *spIm1, 10, 10);
}

//! Testing rectangular image views
TEST_F(ImageDTest, RectangleImageView)
{
    vpl::img::CDImage img(4, 4, 0);
    generateIncreasingImage(img, 0);

    { // 2x2 subimage
        vpl::img::CDImage subimg(img.rect(vpl::img::CPoint2i{ 1, 1 }, vpl::img::CSize2i{ 2, 2 }));
        vpl::img::tDensityPixel expected[] =
        {
            5, 6,
            9, 10
        };
        compare.values(expected, subimg, subimg.getXSize(), subimg.getYSize());
    }

    { // 3x2 subimage
        vpl::img::CDImage subimg(img.rect(vpl::img::CPoint2i{ 1, 0 }, vpl::img::CSize2i{ 3, 2 }));
        vpl::img::tDensityPixel expected[] =
        {
            1, 2, 3,
            5, 6, 7,
        };
        compare.values(expected, subimg, subimg.getXSize(), subimg.getYSize());
    }
}

}