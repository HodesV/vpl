//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include "VPL/Base/Factory.h"

//============== Definitions for test class objects ============================
//! Object CA
namespace factory
{
std::string requiredOutput;

class Ca
{
public:
    //! Default constructor
	Ca() { requiredOutput += "CA::CA()#"; }

    //! Destructor
	virtual ~Ca() { requiredOutput += "CA::~CA()#"; }

    //! Virtual method
	virtual void print() { requiredOutput += "CA::print()#"; }

};
//! Object CAA
class Caa : public Ca
{
public:
    //! Destructor
	~Caa() { requiredOutput += "CAA::~CAA()#"; }

    //! Virtual method
	void print() override { requiredOutput += "CAA::print()#"; }

public:
    //! Object cretation function
    static Ca *create() { return new Caa; }

private:
    //! Default constructor
	Caa() { requiredOutput += "CAA::CAA()#"; }

};


//! Object CAB
class Cab : public Ca
{
public:
    //! Destructor
	~Cab() { requiredOutput += "CAB::~CAB()#"; }

    //! Virtual method
	void print() { requiredOutput += "  CAB::print()#"; }

public:
    //! Object cretation function
    static Ca *create() { return new Cab; }

private:
    //! Default constructor
	Cab() { requiredOutput += "CAB::CAB()#"; }

};
typedef vpl::base::CFactory<Ca, std::string> t;

//============== Test code =========================

//! Register and unregistration only one class.
TEST(FactoryTest,SingleRegistration)
{
    ASSERT_TRUE(VPL_FACTORY(t).registerObject("CAA", Caa::create));
    ASSERT_FALSE(VPL_FACTORY(t).registerObject("CAA", Cab::create));
	ASSERT_TRUE(VPL_FACTORY(t).unregisterObject("CAA"));
}
//! Register and unregistration multiple class.
TEST(FactoryTest, MultipleRegistration)
{
    ASSERT_TRUE(VPL_FACTORY(t).registerObject("CAA", Caa::create));
    ASSERT_TRUE(VPL_FACTORY(t).unregisterObject("CAA"));

    ASSERT_TRUE(VPL_FACTORY(t).registerObject("CAB", Caa::create));
    ASSERT_FALSE(VPL_FACTORY(t).unregisterObject("CAA"));
    ASSERT_TRUE(VPL_FACTORY(t).unregisterObject("CAB"));
}

//! Testing call hiearchy when object is constructed.
TEST(FactoryTest, Creation)
{
	//! Exception must be throw, not registered object
    ASSERT_ANY_THROW(VPL_FACTORY(t).create("CAB"));

	//! Register object
    ASSERT_TRUE(VPL_FACTORY(t).registerObject("CAB", Cab::create));

	//! Create object CAB
    Ca *pCA = VPL_FACTORY(t).create("CAB");

	//! Check call hiearchy
	ASSERT_STREQ("CA::CA()#CAB::CAB()#", requiredOutput.c_str());
	requiredOutput.clear();

    ASSERT_TRUE(pCA != nullptr);

	//! Checks proper destructor hiearchy
    delete pCA;
	ASSERT_STREQ("CAB::~CAB()#CA::~CA()#", requiredOutput.c_str());
	requiredOutput.clear();


	//! Create object CAA
	ASSERT_ANY_THROW(VPL_FACTORY(t).create("CAA"));
	ASSERT_TRUE(VPL_FACTORY(t).registerObject("CAA", Caa::create));

    pCA = VPL_FACTORY(t).create("CAA");
	ASSERT_STREQ("CA::CA()#CAA::CAA()#", requiredOutput.c_str());
	requiredOutput.clear();

    ASSERT_TRUE(pCA != nullptr);
	//! Checks proper destructor hiearchy
	delete pCA;
	ASSERT_STREQ("CAA::~CAA()#CA::~CA()#", requiredOutput.c_str());
	requiredOutput.clear();

    ASSERT_TRUE(VPL_FACTORY(t).unregisterObject("CAB"));

    ASSERT_ANY_THROW(VPL_FACTORY(t).create("CAB"));
}
} // namespace factory