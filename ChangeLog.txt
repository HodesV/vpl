================================================================================
- This file is part of
- 
- VPL - Voxel Processing Library
- Copyright 2014 3Dim Laboratory s.r.o.
- All rights reserved.
- 
- Use of this file is governed by a BSD-style license that can be
- found in the LICENSE file.
================================================================================

VPL 1.3.0 (2017/mm/dd):
  - Fixed bug in CBlockChannel.
  - Eigen library updated to version 3.2.4.
  - This is the first version of VPL derived from the original MDSTk toolkit.
  - VectorEntity library has been depricated and removed.
  - CMakeLists.txt modified to be able to use system Eigen library
    instead of the supplied one.
  - Added support for 64-bit compilation on Windows using MSVC.
  - Added Mac OS X support.
  - Improved logging interface with more levels and more capabilities.

----------------------------------------

MDSTk Version 1.2.1 (2012/mm/dd):
  - Added support for serialization of STL containers like std::vector<>,
    std::set<> and std::basic_string<>.
  - Added preliminary support for Mac OS X!

MDSTk Version 1.1.1 (2012/04/07):
  - Fixed bug in installation of Eigen header files.
  - File mdsString.cpp has been correctly added to the libSystem library.
  - Fixed bug in RGBA pixel definition (i.e. the CRGBA class).
  - Found and fixed bug in calculation of a matrix inverse using Eigen.
  - Fixed bugs in CVolume::create() and CImage::create() methods that cause
    wrong initialization of data when a small subvolume was copied out.
  - Fixed bug in CRectBox<>::copyTo() and CRectBox<>::copyFrom() methods.
  - A modified version of the MDS_UNUSED() macro.
  - Fixed bug in mdsAnisotropic.hxx (parallelization via OpenMP).
  - Fixed compilation using MinGW on Windows.

MDSTk Version 1.1.0 (2012/02/19):
  - Math library has been ported from Eigen2 to Eigen3!
  - Added preliminary support for Unicode filenames.
  - Changes in the iterator concept that result in its simplification
    and better performance.   
  - Added OpenMP support.
  - Improvements of CImageBase<>, CVolumeBase<>, CMatrixBase<> and other class
    templates that implements the curiously recurring template pattern.
  - New class templates CRect<>, CRectBox<>, CImageRow<> and CVolumeRow<>
    providing views of images and volumes.
  - Improved support for forEach() concept.
  - Added CArray<> class template that provides the same functionality
    like the old-style CVector<>. It allows to make references to existing
    vectors.
  - Added a new data allocator - CPartedData<> - able to allocate large
    volumetric data in smaller blocks.
  - Upgraded to newer versions of several 3rd party libraries (i.e. zlib,
    libpng, freeglut, FFTW).
  - All get(i,j) methods returning reference to a matrix coefficient,
    vector element, image pixel, etc. were newly replaced by at(i,j) methods.
  - New CPartedData<> allocator able to allocate a large block of memory
    in parts. This is the default allocator for CVolume<> from now.
  - FindOpenCV2.cmake has been modified so that it supports OpenCV 2.3.1. 

MDSTk Version 1.0.2 (2010/11/06):
  - Fixed bug in 'FindMDSTk.cmake' file.
  - Added new modules for loading and saving sequences of images from/to
    specified directories (see mdsLoadJPEGDir, mdsLoadDicomDir, etc.).
  - Fixed bug in the experimental OpenCV support. An obsolete macro
    still used in the code caused errors during the compilation...
  - The 'FindOpenCV.cmake' file was modified so that the latest OpenCV 2.1
    is supported and works fine with the toolkit.
  - Description of all modules was reviewed.
  - Number of minor changes and bugfixes.

MDSTk Version 1.0.1 (2010/09/02):
  - Fixed bugs in serialization methods of the vctl::MCVertex class.
  - Added missing specializations of the mds::lbp::CLBPTraits class.
  - Found bug in the mds::img::CvImage<>::convert() method.
  - Corrected and improved functionality of the mds::img::CAdjacencyGraph class.
  - Directory 'lib' as well as prebuilt GLUT library were removed from
    the MDSTk distribution pack. Packages of all required prebuilt 3rd party
    libraries can be downloaded separately from now.
  - Modified 'CMakeLists.txt' in order to simplify the compilation process.
  - Added pkgconfig files detailing MDSTk libraries.
  - Added sample scripts running MDSTk modules (see the 'temp' directory).
  - Solved problem with dereferencing of a wrapped STL iterator during
    comparison of two iterators. Such dereferenceng may result in runtime
    error in case of the end() iterator on some platforms.
  - Several minor changes and bugfixes.

MDSTk Version 1.0.0rc2 (2010/02/09):
  - Fixed bug in the installation process that omits to copy header files
    of the Eigen math library.
  - For better backward compatibility, previous version of matrix and vector
    classes (before moving towards the Eigen library) has been added
    to the mds::math::ver1 namespace.
  - Corrected bug in the median image/volume filter incurred by changes
    in the math library. The matrix, as well as vector, class template
    can be instantiated only for "fundamental" types like unsigned int, float,
    double, std::complex<>, etc.
  - Fixed bugs in the vctl::MCTransformMatrix class.

MDSTk Version 1.0.0rc (2010/01/27):
  - Many changes in the math library! The library is based on open source
    libraries Eigen and UMFPACK from now. Dependency on the ATLAS library
    was removed.
  - A new experimental support for the OpenCV library. You can take advantage
    of using both libraries within a one project!
    See the mds::img::CVImage<> class template for details.
  - Many changes in the CMake build system. Installed system libraries
    libJPEG, libPNG, etc. can be used instead of ones supplied with MDSTk
    toolkit.
  - Naming convention of built MDSTk libraries has been changed to
    mdsXYZ.lib, libmdsXYZ.a, etc. format.
  - Some modifications and tuning of the EM algorithm have improved
    its functionality.
  - Few changes in the concept of container iterators
    (added mds::base::CIteratorTraits and mds::base::CIteratorBase class
    templates.
  - Fixed bug in the mds::base::CSTLIterator wrapper of STL iterators
    (dereferencing of end() interator in the constructor).
  - The toolkit can be built for 64-bit environment on Linux platform.
  - Many bugfixes and minor changes!

MDSTk Version 0.8.2beta (2010/01/04):
  - Corrected an awful bug in object reference counting provided
    by the mds::base::CObject class!
  - Fixed bugs in mds::mod::CBlockChannel and mds::mod::CGZipCompressor
    that caused failure during an RGB image deserialization.
  - Several changes in the mds::sys::CNamedPipe class. Now, channels over named
    pipes are fully functional on windows.

MDSTk Version 0.8.1beta (2009/06/02):
  - A special lossless compression method of medical density data
    has been added.
  - Modified template-based implementation of the serialization interface.
  - Experimental XML support via the TinyXML library added. A newly coded
    XML serializer allows you to load/save all serializable objects
    from/to text XML file.
  - Added functions for bicubic and Lanczos image interpolation.
  - Added module providing affine geometrical image transforms.
  - Re-organized hierarchy of the Image library.
  - Added implementation of the Fast Watershed Transform algorithm.
  - Sample module providing image segmentation based on watershed transform
    and subsequent region merging.
  - Added a new module option used to change filename of the output log.
  - Implementation of well known LBP (Local Binary Pattern) texture
    features (+ sample modules).
  - Macro MDS_CLASS_NAME() was removed from the mds::base::CObject class.
    A new class mds::base::CNamedObject has been added.
  - Changes in the VectorEntity library (english comments ;-).
  - Minor changes and bugfixes.

MDSTk Version 0.8.0beta (2008/07/29):
  - Added support for simple progress monitoring. Channel serializer
    is now able to notify calling process about the current state of
    a running I/O operation.
  - New definition of a base MDSTk runtime exception class.
    See the mds::mod::CSerializer class that shows the prefered way
    how exceptions can be declared, thrown and handled.
  - Few changes in CMakeLists.txt files. The newest CMake version 2.6 may
    be used for now.
  - Added implementation of the Lucas-Kanade optical flow estimation method.
  - Modified definition of image/volume filters hoping that the class hierarchy
    is more transparent.
  - Added some basic methods for image histogram thresholding (iterative
    thresholding with multiple thresholds, Otsu's algorithm, etc.)
  - Certain new components (e.g. implementation of the Connected Component
    Labeling algorithm and class encapsulating the Region Adjacency Graph).
  - Improved implementation of the region growing segmentation
    (see the SliceSegRG module).
  - Other minor changes and bugfixes.

MDSTk Version 0.7.2beta (2008/03/04):
  - Corrected default settings used to link against the OpenGL and GLUT library
    on linux. The old ones didn't work on some linux systems.
  - Added simple implementation of functors and first version of signals
    whose concept was partially inspired with the Boost library.
  - Global signals represented as singletons suitable for distribution
    of events within an application.
  - Modified CMake rules so that all header files are present in generated
    MSVC projects.
  - All necessary DLL libraries are copied to a predefined destination
    directory during installation step.
  - Changed meaning of pixels in density medical images so that CT image
    pixels imported from DICOM format represents Hounsfiled units [Hu].
    Hence, negative values are allowed too (values in the range -1500..7000
    are permitted). See include/MDSTk/Image/mdsPixelTraits.h for details.
  - Fixed bug in the mdsSliceSubsampling module.

MDSTk Version 0.7.1beta (2007/12/03):
  - Added 'install' target which copies all header files, compiled libraries
    and modules to a predefined installation directory.
  - Corrected few bugs in implementation of smart pointers.
  - Singleton lifetime tracker has been modified with respect to some actual
    changes in the Loki library. This corrects few problems of the previous
    version.
  - Corrected bugs in specialization of CTypeTraits<T>::getMax() and
    CTypeTraits<T>::getMin() template methods.

MDSTk Version 0.7.0beta (2007/09/28):
  - Build system completely changed to the CMake framework. Former manually
    prepared Makefiles and MSVC solutions have been removed. 
  - Changes in directory structure of the toolkit regarding the CMake system
    philosophy.
  - Added typical 'configure.h' header file...

MDSTk Version 0.6.1beta (not officially released):
  - Added Harris & Stephens image corner detection algorithm. New modules
    providing 2D and 3D corner detection.
  - Corrected bug in the JPEG library on windows platform (definition
    of the boolean type in 'rpcndr.h' system header file differs
    from the original one).
  - Added support for the excellent CMake (Cross-platform Make) build system.
    Many thanks go to Mirek Svub.
  - Others minor changes and bugfixes.

MDSTk Version 0.6.0beta (2007/06/22):
  - Many changes in data entity serialization over channels. New binary
    serializer as well as SHM serializer added. Images and volumes supports
    direct sharing of data over SHM for now. So, its not necessary to copy
    any memory during the deserialization of image/volume from shared memory.
  - Reorganized ImageIO library to allow loading of JPEG and PNG
    images into different types of images (e.g. RGB and grayscale). Removed 
    classes CJPEGSlice and CPNGSlice for loading PNG and JPEG images as
    medical slices. Added universal functions loadPNG(), savePNG(), etc.
    which are able to read/write any MDSTk image from/to PNG or JPEG image.
  - Modified code of basic and cumulative histogram computation.
  - Added new modules for image/volume histogram equalization.
  - Added new iterator's base classes.
  - Modified spirit of smart pointer initialization.
  - Added elementary support for sparse matrices and sparse system solver
    that uses popular UMFPACK library.
  - New parameters of mdsLoadJPEG and mdsLoadPNG modules.
  - Others minor changes and bugfixes.

MDSTk Version 0.5.3beta (2007/01/03):
  - Disabled security warnings generated by the MS Visual C++ 2005 compiler.
  - Modified serialization approach of data entities. It is not neccessary
    to count accurate size of stored data because a block channel organizing
    data into fixed sized blocks is used for now.
  - Many changes in the VectorEntity library!
  - Several bugs fixed.

MDSTk Version 0.5.2beta (2006/11/09):
  - Added full LAPACK library. The optimized ATLAS library implementing BLAS
    routines and few LAPACK functions and the full CLAPACK library are linked
    together. You can use routines from both libraries thenceforth.
    Pre-compiled library is optimized for the Pentium II processor and
    it doesn't use any SSE or 3DNow instructions. Performance of the library
    is not optimal but it is more portable.
  - Function for computation of eigenvalues and eigenvectors of a square
    symmetric matrix.
  - Added first version of a brief MDSTk guide.
  - Others minor changes.

MDSTk Version 0.5.1beta (2006/09/27):
  - Added CLogNum class representing numbers in logarithmic space. All
    arithmetic operations like addition and multiplication are performed in the
    logarithmic space.
  - General implementation of two clustering algorithms: Fuzzy C-Means and
    Gaussian Mixture Models (GMMs) optimized using the Expectation-Maximization
    (EM) algorithm. Input incomplete data are feature vectors, or just numbers,
    stored in a container which must provide iterators to pass through.
  - Several bugs fixed.

MDSTk Version 0.5.0beta (2006/08/11):
  - Completely remade directory structure of the MDSTk toolkit. All required
    header files can be found in the /include directory and source files
    in the /src directory for now.
  - P. Krsek's library called VectorEntity has been added to the MDSTk toolkit.
    This library provides a number of vector graphics functions. It containes
    classes for vertices, edges, triangles, tetrahedrons, ... in 3D space.
  - Modified approach of data entities. Every data entity provides simple
    serialization and deserialization interface that allows to use inheritance
    and hierarchy of the data entities.
  - Added support for complex images as well as fundamental image conversion
    functions.
  - New functions for computation of two-dimensional Discrete Fourier Transform
    (DFT) which uses the well known FFTW library. Note that the library
    is distributed under the GNU General Public License which is more
    restrictive than the license of MDSTk toolkit.
  - Few modifications of the image/volume filtering approach which may improve
    its usability. Removed several bugs in implementations.
  - Many others minor changes and bugfixes.

MDSTk Version 0.4.1beta (2006/07/12 not officially released):
  - Several minor changes in interface of communication channels. The method
    CChannel::isClosed() was replaced by the CChannel::isConnected() function.
  - Communication channels via the shared memory have been added. The
    implementation is very simple and only for testing. It should be remade
    in future.
  - 16-bit RLE compression is supported.
  - Class CConsole provides only basic functionality required by any console
    application. More MDSTk specific functions are given in the classes CModule
    and CView which are derived from the CConsole.
  - New utility named mdsCreateSHM can be used to allocate a shared memory
    block. This utility is necessary if you want to use shared memory channels.
  - New class CComplex<> representing complex numbers which is analogous to the
    std::complex<> class. 
  - Square min and max image and volume filters.
  - Many others changes and bugfixes.

MDSTk Version 0.4.0beta (2006/02/25):
  - New template based library libMath including matrix and vector classes.
    Fast numerical rutines for linear algebra, e.g. matrix factorization and
    inverse, are provided through the use of the ATLAS library.
  - Modified design of the CImage and CVolume classes. Methods such as getMin(),
    getVariance() and convolve() were removed and reimplemented as global
    functions.
  - Many others minor changes and bugfixes.

MDSTk Version 0.3.2beta (2006/01/26):
  - Modified mds::CChannel class representing I/O channels. Global functions
    mds::chCreate() and mds::chSeparate() were moved into the class CChannel.
    Now these functions are static members of the class.
  - Corrected horrible bug in the method mds::CDicomSlice::loadDicom() which
    causes huge memory leaks!
  - Improved DICOM medical image format parsing provided by the
    mds::img::CDicomSlice class. Added new attributes like slice tilt.
  - Modified Doxygen projects (version 1.4.5) for automatic generation of the
    source code documentation.
  - Clases and others sources were divided into individual namespaces. There
    are new namespaces called mds::base (most of the libBase library), mds::img
    (libImage and libImageIO), mds::mod (libModule) and finally mds::sys
    (libSystem).
  - Removed header file libModule/mdsModule.h. Its content has been moved
    to the libBase/mdsSetup.h file.
  - Few changes in the way of running MDSTk modules. Console module (class
    mds::mod::CConsole) doesn't execute separate threads for each input channel.
    Just a one processing thread is used which can be cancelled by ^C key
    sequence. The class mds::mod::CView works in the same way.
  - System singleton mds::base::CSystemSingleton has been renamed to the
    mds::base::CLibrarySingleton.
  - All MDSTk libraries can be compiled as singlethreaded (no locking) or
    multithreaded. Singlethreaded version is faster, therefore is prefered.
    You can switch between both versions by defining macro MDS_MULTITHREADED
    in the libBase/mdsSetup.h header file.

MDSTk Version 0.3.1beta (2005/12/23):
  - Toolkit has been completely ported to the MS Visual C++ compiler. Required
    compiler version is 7.1 or higher.
  - New small objects allocator implementation based on actual version of the
    Loki library.
  - Extended functionality of image and volume iterators. Constant objects
    can be tranversed using const iterators. Iterator can be initialized to
    point at any pixel position.
  - Modified internal matrix representation which is now similar to that
    of images.
  - Matrix operations like matrix inverse computation are excluded from
    matrix class. Hence, they could be used for both static and dynamic
    matrixes.
  - All testing programs are distributed together with the toolkit now. Even
    though they are very simple, you can use them for studying.
  - Added new modules.

MDSTk Version 0.3.0beta (2005/mm/dd):
  - Completely remade makefiles and whole compilation. All object files are
    stored in a separate directory, so they don't disturb you in directories
    containing source files. Full dependencies can be now generated using
    'make depend' command.
  - Modified logging interface and functionality. Classes representing global
    application log and log channels were defined. Now, you can make and
    register your own output logging channels.
  - Macros MIN, MAX, ABS and etc. were replaced by template functions having
    same meaning. The old macros are still there but marked as obsolete.
  - Added operator '<' (less) for smart pointers comparison.
  - New built-in pixel/image types such as RGB image and so on. Added templates
    for pixel traits and pixel conversions. Image(volume) pixel(voxel) range
    is a global property now. You can't set it individually for an image. On the
    other side a method 'convert(...)' has been added that allows you to convert
    images of different types to each other.
  - Pixel types 'tFPixel' and 'tUPixel' have been renamed to 'tFloatPixel' and
    'tDensityPixel'.
  - Classes mds::CUImage (usnigned short/density image) and mds::CDVOlume are
    renamed to mds::CDImage and mds::CDVolume as well as its pixel types.
  - Modified templates and classes for image/volume filtering. Every filter
    is represented by a specialized generic filter template. Many basic filters
    such as Gaussian, Median, Sobel and Prewit operators and so on have been
    added.
  - Removed modules mSlice(Volume)MedianFilter. Median filtering is now provided
    by the mSlice(Volume)Filter module now.
  - Advanced edge detection algorithms were added. E.g. Canny Edge Detector,
    DoG (Difference of Gaussians) and LoG Zero Crossings.
  - Corrected bug in the 'mVolumeCut' module which causes the module
    malfunction.
  - Many others not so important changes and corrected bugs.

MDSTk Version 0.2.3beta (2005/07/20):
  - Support for identification of objects on the heap was added.
    Objects created via 'new' operator have references counter initialized
    to zero, statically created object always have at least one reference.
    On the other hand, small object allocation model and intrusive reference
    (as well as smart pointers) can't be used together.
  - Few modifications of the CRefData<>, CData<>, CImage<> and CVolume<>
    templates.

MDSTk Version 0.2.2beta (not officially released):
  - The GNU GCC compiler option '-ansi' was removed from all makefiles. It seems
    that this compilation option causes that GCC v3.4.x (and above) cannot
    compile the sources.

MDSTk Version 0.2.1beta (2005/03/07):
  - Fixed bug in the mdsSliceView module. It crashed if there was no
    successfully read input slice.
  - Corrected the module mdsLoadDICOM that fails to read DICOM data
    having the explicit transfer set to true.
  - Camera initialization problem when viewing a large volume data has been
    repaired. Fixed in the mdsVolumeView module.
  - Fixed bug in the mds::CError class.
  - Added a simple version of an object factory template mds::CFactory.
  - The data entities concept has been simplified and extended by
    a possibility to use data compression.
  - RLE channel compression has been added.

MDSTk Version 0.2.0beta (2005/02/14):
  - Many changes in the libBase library. Added smart pointers, singletons,
    small object allocators and so on. Image and volume iterators are now ready
    to use. Improved type traits.
  - New synchronization and system tools has been added to the libSystem
    Library, e.g. CSemaphore, CFileBrowser, CSharedMem and CStopwatch.
  - Several new modules used to manipulate slices and volume has been added.
  - New modules: mSliceHistogram, mSliceMedianFilter, mSliceCut,
    mSliceTresholding, mVolumeHistogram, mVolumeFilter, mVolumeMedianFilter,
    mVolumeCut and mVolumeTresholding.

MDSTk Version 0.1.3beta (2004/08/10):
  - Several changes in the libBase and libImage library.
  - Several modules used to manipulate volume has been added.
  - New modules: mMakeVolume, mVolumeSplit, mVolumeInfo, mVolumeRange
    and mVolumeView.

MDSTk Version 0.1.2beta (2004/05/03):
  - Implemented channels via named pipes.
  - Module mSliceView uses alpha blending to combine several slices together.
  - New modules: mSliceFilter.

MDSTk Version 0.1.1beta (2004/04/13):
  - Normalized output of the module mdsSliceRange, pixel values
    are in the range 0..255.
  - Module mdsSliceView uses the same pixel format as mentioned above.
  - Added support for loading/saving images in JPEG and PNG format.
  - New libraries: libImageIO.
  - New modules: mLoadJPEG, mLoadPNG, mSaveJPEG and mSavePNG.
  - Several bugs fixed.

MDSTk Version 0.1.0beta (2003/12/24):
  - First version of a basic libraries and modules.
  - Libraries: libBase, libSystem, libImage and libModule.
  - Modules: mLoadDicom, mSliceRange, mSliceInfo and mSliceView.
