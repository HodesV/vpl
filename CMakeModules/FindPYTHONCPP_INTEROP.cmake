# CMake module that tries to find Python extension includes and libraries.
#
# The following variables are set if Python extension is found:
#   PYTHONCPP_INTEROP_FOUND         - True when the Python extension include directory is found.
#   PYTHONCPP_INTEROP_INCLUDE_DIR   - The directory where Python extension include files are.
#   PYTHONCPP_INTEROP_LIBRARIES_DIR - The directory where Python extension libraries are.
#   PYTHONCPP_INTEROP_LIBRARIES     - List of all Python extension libraries.
#
# Usage:
#   In your CMakeLists.txt file do something like this:
#   ...
#   FIND_PACKAGE(PYTHONCPP_INTEROP)
#   ...
#   INCLUDE_DIRECTORIES(${PYTHONCPP_INTEROP_INCLUDE_DIR})
#   LINK_DIRECTORIES(${PYTHONCPP_INTEROP_LIBRARIES_DIR})
#   ...
#   TARGET_LINK_LIBRARIES( mytarget ${PYTHONCPP_INTEROP_LIBRARIES} )


# Initialize the search path
if( WIN32 )
  set( PYTHONCPP_INTEROP_DIR_SEARCH
       "${TRIDIM_3RDPARTY_DIR}"
       "${TRIDIM_MOOQ_DIR}"
       "${PYTHONCPP_INTEROP_ROOT_DIR}"
       "$ENV{PYTHONCPP_INTEROP_ROOT_DIR}"
       )
  set( PYTHONCPP_INTEROP_SEARCHSUFFIXES
       include
       )
else()
  set( PYTHONCPP_INTEROP_DIR_SEARCH
       "${TRIDIM_MOOQ_DIR}"
       "${PYTHONCPP_INTEROP_ROOT_DIR}"
       /usr/include
       /usr/local/include
       /opt/include
       /opt/local/include
       )
  set( PYTHONCPP_INTEROP_SEARCHSUFFIXES
       ""
       )
endif()


# Try to find PYTHONCPP_INTEROP include directory
find_path( PYTHONCPP_INTEROP_INCLUDE_DIR
           NAMES
           PyLib/runtime.h
           PATHS
           ${PYTHONCPP_INTEROP_DIR_SEARCH}
           PATH_SUFFIXES
           ${PYTHONCPP_INTEROP_SEARCHSUFFIXES}
           )


# Assume we didn't find it
set( PYTHONCPP_INTEROP_FOUND NO )
# Now try to get the library path
IF( PYTHONCPP_INTEROP_INCLUDE_DIR )

  # Look for the PYTHONCPP_INTEROP library path
  SET( PYTHONCPP_INTEROP_LIBRARIES_DIR ${PYTHONCPP_INTEROP_INCLUDE_DIR} )

  # Strip off the trailing "/include" in the path
  IF( "${PYTHONCPP_INTEROP_LIBRARIES_DIR}" MATCHES "/include$" )
    GET_FILENAME_COMPONENT( PYTHONCPP_INTEROP_LIBRARIES_DIR ${PYTHONCPP_INTEROP_LIBRARIES_DIR} PATH )
  ENDIF( "${PYTHONCPP_INTEROP_LIBRARIES_DIR}" MATCHES "/include$" )

  # Check if the 'lib' directory exists
  IF( EXISTS "${PYTHONCPP_INTEROP_LIBRARIES_DIR}/lib" )
    SET( PYTHONCPP_INTEROP_LIBRARIES_DIR ${PYTHONCPP_INTEROP_LIBRARIES_DIR}/lib )
  ENDIF( EXISTS "${PYTHONCPP_INTEROP_LIBRARIES_DIR}/lib" )
    

  

  # Add unversioned image lib name to the testing list
  list(APPEND PYTHONCPP_INTEROP_PYTHONLIB_NAMES PythonCpp)
  list(APPEND PYTHONCPP_INTEROP_PYTHONLIB_DEBUG_NAMES PythonCppd)
 
  

  # Find release libraries
  FIND_LIBRARY( PYTHONCPP_INTEROP_libPython_LIBRARY
                NAMES ${PYTHONCPP_INTEROP_PYTHONLIB_NAMES}
                PATHS ${PYTHONCPP_INTEROP_LIBRARIES_DIR} ${PYTHONCPP_INTEROP_LIBRARIES_DIR_RELEASE}
                NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH )

  # Try to find debug libraries
  FIND_LIBRARY( PYTHONCPP_INTEROP_libPythond_LIBRARY
                NAMES ${PYTHONCPP_INTEROP_PYTHONLIB_DEBUG_NAMES}
                PATHS ${PYTHONCPP_INTEROP_LIBRARIES_DIR} ${PYTHONCPP_INTEROP_LIBRARIES_DIR_DEBUG}
                NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH )
  
  unset(PYTHONCPP_INTEROP_PYTHONLIB_NAMES)
  unset(PYTHONCPP_INTEROP_PYTHONLIB_DEBUG_NAMES)
  
               
  # Check what libraries was found
  IF( PYTHONCPP_INTEROP_libPython_LIBRARY AND PYTHONCPP_INTEROP_libPythond_LIBRARY )
    SET( PYTHONCPP_INTEROP_LIBRARIES
         optimized PythonCpp
         debug PythonCppd)
    SET( PYTHONCPP_INTEROP_FOUND "YES" )
    
  ELSE( PYTHONCPP_INTEROP_libPython_LIBRARY AND PYTHONCPP_INTEROP_libPythond_LIBRARY )
 
    IF(PYTHONCPP_INTEROP_libPython_LIBRARY)
      SET( PYTHONCPP_INTEROP_LIBRARIES PythonCpp )
      SET( PYTHONCPP_INTEROP_FOUND "YES" )
      
    ELSE( PYTHONCPP_INTEROP_libPython_LIBRARY )
    
      IF( PYTHONCPP_INTEROP_libPythond_LIBRARY )
        SET( PYTHONCPP_INTEROP_LIBRARIES  PythonCppd)
        SET( PYTHONCPP_INTEROP_FOUND "YES" )
      ENDIF( PYTHONCPP_INTEROP_libPythond_LIBRARY )
      
    ENDIF( PYTHONCPP_INTEROP_libPython_LIBRARY )
    
  ENDIF( PYTHONCPP_INTEROP_libPython_LIBRARY AND PYTHONCPP_INTEROP_libPythond_LIBRARY )
  
ENDIF( PYTHONCPP_INTEROP_INCLUDE_DIR )

# Using shared or static library?
IF( PYTHONCPP_INTEROP_FOUND )
#    OPTION( PYTHONCPP_INTEROP_USE_SHARED_LIBRARY "Should shared library be used?" OFF )
#    IF( PYTHONCPP_INTEROP_USE_SHARED_LIBRARY )
#      ADD_DEFINITIONS( -DMDS_LIBRARY_SHARED )
#    ELSE( PYTHONCPP_INTEROP_USE_SHARED_LIBRARY )
      ADD_DEFINITIONS( -DMDS_LIBRARY_STATIC )
#    ENDIF( PYTHONCPP_INTEROP_USE_SHARED_LIBRARY )
    if( APPLE )
        add_definitions( -D_MACOSX )
    else()
        if( UNIX )
            add_definitions( -D_LINUX )
        endif()
    endif()
ENDIF( PYTHONCPP_INTEROP_FOUND )

MARK_AS_ADVANCED( PYTHONCPP_INTEROP_INCLUDE_DIR
                  PYTHONCPP_INTEROP_libPython_LIBRARY
                  PYTHONCPP_INTEROP_libPythond_LIBRARY )


# display help message
IF( NOT PYTHONCPP_INTEROP_FOUND )
  # make FIND_PACKAGE friendly
  IF( NOT PYTHONCPP_INTEROP_FIND_QUIETLY )
    IF( PYTHONCPP_INTEROP_FIND_REQUIRED )
      MESSAGE( FATAL_ERROR "PYTHONCPP_INTEROP required but some headers or libs not found. Please specify it's location with PYTHONCPP_INTEROP_ROOT_DIR variable.")
      SET( PYTHONCPP_INTEROP_EXTENSION_ROOT_DIR "" CACHE PATH "PYTHONCPP_INTEROP root dir" )
    ELSE( PYTHONCPP_INTEROP_FIND_REQUIRED )
      MESSAGE(STATUS "ERROR: PYTHONCPP_INTEROP was not found.")
    ENDIF( PYTHONCPP_INTEROP_FIND_REQUIRED )
  ENDIF( NOT PYTHONCPP_INTEROP_FIND_QUIETLY )
ENDIF( NOT PYTHONCPP_INTEROP_FOUND )

