# CMake module that tries to find RapidJSON includes and libraries.
#
# The following variables are set if RapidJSON is found:
#   RAPIDJSON_FOUND         - True when the RapidJSON include directory is found.
#   RAPIDJSON_INCLUDE_DIR   - The directory where RapidJSON include files are.
#   RAPIDJSON_LIBRARIES_DIR - The directory where RapidJSON libraries are.
#   RAPIDJSON_LIBRARIES     - List of all RapidJSON libraries.
#
# Usage:
#   In your CMakeLists.txt file do something like this:
#   ...
#   FIND_PACKAGE(RapidJSON)
#   ...
#   INCLUDE_DIRECTORIES(${RAPIDJSON_INCLUDE_DIR})
#   LINK_DIRECTORIES(${RAPIDJSON_LIBRARIES_DIR})
#   ...
#   TARGET_LINK_LIBRARIES( mytarget ${RAPIDJSON_LIBRARIES} )


# Initialize the search path
if( WIN32 )
  set( RAPIDJSON_DIR_SEARCH
       "${VPL_3RDPARTY_DIR}/include/rapidjson/"
       "${TRIDIM_MOOQ_DIR}/include/rapidjson/"
       "${RAPIDJSON_ROOT_DIR}"
       "$ENV{RAPIDJSON_ROOT_DIR}"
       )
  set( RAPIDJSON_SEARCHSUFFIXES
       ""
       )
else()
  set( RAPIDJSON_DIR_SEARCH
       "${TRIDIM_MOOQ_DIR}"
       "${RAPIDJSON_ROOT_DIR}"
       /usr/include/rapidjson
       /usr/local/include/rapidjson
       /opt/include/rapidjson
       /opt/local/include/rapidjson
       )
  set( RAPIDJSON_SEARCHSUFFIXES
       ""
       )
endif()


# Try to find RAPIDJSON include directory
find_path( RAPIDJSON_INCLUDE_DIR
           NAMES
           rapidjson.h
           PATHS
           ${RAPIDJSON_DIR_SEARCH}
           PATH_SUFFIXES
           ${RAPIDJSON_SEARCHSUFFIXES}
           )

# Assume we didn't find it
set( RAPIDJSON_FOUND NO )
# Now try to get the library path
IF( RAPIDJSON_INCLUDE_DIR )
    SET( RAPIDJSON_FOUND "YES" )
ENDIF()

MARK_AS_ADVANCED( RAPIDJSON_INCLUDE_DIR
                  RAPIDJSON_LIBRARY
                  RAPIDJSON_DEBUG_LIBRARY )

# display help message
IF( NOT RAPIDJSON_FOUND )
  # make FIND_PACKAGE friendly
  IF( NOT RapidJSON_FIND_QUIETLY )

    IF( RapidJSON_FIND_REQUIRED )
      SET( RAPIDJSON_ROOT_DIR "" CACHE PATH "RapidJSON root dir")
      MESSAGE( FATAL_ERROR "RapidJSON required but some headers or libs not found. Please specify it's location with RAPIDJSON_ROOT_DIR variable.")
    ELSE()
      MESSAGE(STATUS "ERROR: RapidJSON was not found.")
    ENDIF()
  ENDIF()
else()
unset(RAPIDJSON_ROOT_DIR CACHE)
ENDIF()

