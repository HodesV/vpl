# CMake module that tries to find TINYXML2 includes and libraries.
#
# The following variables are set if TINYXML2 is found:
#   TINYXML2_FOUND         - True when the TINYXML2 include directory is found.
#   TINYXML2_INCLUDE_DIR   - The directory where TINYXML2 include files are.
#   TINYXML2_LIBRARIES_DIR - The directory where TINYXML2 libraries are.
#   TINYXML2_LIBRARIES     - List of all TINYXML2 libraries.
#
# Usage:
#   In your CMakeLists.txt file do something like this:
#   ...
#   FIND_PACKAGE(TINYXML2)
#   ...
#   INCLUDE_DIRECTORIES(${TINYXML2_INCLUDE_DIR})
#   LINK_DIRECTORIES(${TINYXML2_LIBRARIES_DIR})
#   ...
#   TARGET_LINK_LIBRARIES( mytarget ${TINYXML2_LIBRARIES} )


# Initialize the search path
if( WIN32 )
  set( TINYXML2_DIR_SEARCH
       "${TRIDIM_3RDPARTY_DIR}"
       "${TRIDIM_MOOQ_DIR}"
       "${TINYXML2_ROOT_DIR}"
       "$ENV{TINYXML2_ROOT_DIR}"
       )
  set( TINYXML2_SEARCHSUFFIXES "include")
else()
  set( TINYXML2_DIR_SEARCH
       "${TRIDIM_MOOQ_DIR}"
       "${TINYXML2_ROOT_DIR}"
       /usr/include
       /usr/local/include
       /opt/include
       /opt/local/include
       )
  set( TINYXML2_SEARCHSUFFIXES "")
endif()


# Try to find TINYXML2 include directory
find_path( TINYXML2_INCLUDE_DIR
           NAMES
           tinyxml2.h
           PATHS
           ${TINYXML2_DIR_SEARCH}
           PATH_SUFFIXES
           ${TINYXML2_SEARCHSUFFIXES}
           )


# Assume we didn't find it
set( TINYXML2_FOUND NO )
# Now try to get the library path
IF( TINYXML2_INCLUDE_DIR )

    # Look for the TINYXML2 library path
    SET( TINYXML2_LIBRARIES_DIR ${TINYXML2_INCLUDE_DIR} )

    # Strip off the trailing "/include" in the path
    IF( "${TINYXML2_LIBRARIES_DIR}" MATCHES "/include$" )
        GET_FILENAME_COMPONENT( TINYXML2_LIBRARIES_DIR ${TINYXML2_LIBRARIES_DIR} PATH )
    ENDIF()


    if(WIN32)

        IF( EXISTS "${TINYXML2_LIBRARIES_DIR}/lib" )
            SET( TINYXML2_LIBRARIES_DIR ${TINYXML2_LIBRARIES_DIR}/lib )
        endif()
    else()
  
          # Check if the 'lib' directory exists
          IF( EXISTS "${TINYXML2_LIBRARIES_DIR}/lib/x86_64-linux-gnu" )
                SET( TINYXML2_LIBRARIES_DIR ${TINYXML2_LIBRARIES_DIR}/lib/x86_64-linux-gnu )
          ENDIF()   
    endif()
  

  # Add unversioned image lib name to the testing list
  list(APPEND TINYXML2_LIB_NAMES tinyxml2)
  list(APPEND TINYXML2_LIB_DEBUG_NAMES tinyxml2d)
 
  

  # Find release libraries
  FIND_LIBRARY( TINYXML2_LIBRARY
                NAMES ${TINYXML2_LIB_NAMES}
                PATHS ${TINYXML2_LIBRARIES_DIR} ${TINYXML2_LIBRARIES_DIR_RELEASE}
                NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH )

  # Try to find debug libraries
  FIND_LIBRARY( TINYXML2_DEBUG_LIBRARY
                NAMES ${TINYXML2_LIB_DEBUG_NAMES}
                PATHS ${TINYXML2_LIBRARIES_DIR} ${TINYXML2_LIBRARIES_DIR_DEBUG}
                NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH )
  
  unset(TINYXML2_LIB_NAMES)
  unset(TINYXML2_LIB_DEBUG_NAMES)
  
               
  # Check what libraries was found
  IF( TINYXML2_LIBRARY AND TINYXML2_DEBUG_LIBRARY )
    SET( TINYXML2_LIBRARIES
         optimized tinyxml2
         debug tinyxml2d)
    SET( TINYXML2_FOUND "YES" )
    
  ELSE()
 
    IF(TINYXML2_LIBRARY)
      SET( TINYXML2_LIBRARIES tinyxml2 )
      SET( TINYXML2_FOUND "YES" )
      
    ELSE()
    
      IF( TINYXML2_libPythond_LIBRARY )
        SET( TINYXML2_LIBRARIES  tinyxml2d)
        SET( TINYXML2_FOUND "YES" )
      ENDIF()
      
    ENDIF()
    
  ENDIF()
  
ENDIF()

# Using shared or static library?
IF( TINYXML2_FOUND )
#    OPTION( TINYXML2_USE_SHARED_LIBRARY "Should shared library be used?" OFF )
#    IF( TINYXML2_USE_SHARED_LIBRARY )
#      ADD_DEFINITIONS( -DMDS_LIBRARY_SHARED )
#    ELSE( TINYXML2_USE_SHARED_LIBRARY )
      ADD_DEFINITIONS( -DMDS_LIBRARY_STATIC )
#    ENDIF( TINYXML2_USE_SHARED_LIBRARY )
    if( APPLE )
        add_definitions( -D_MACOSX )
    elseif( UNIX )
        add_definitions( -D_LINUX )
    endif()
ENDIF()

MARK_AS_ADVANCED( TINYXML2_INCLUDE_DIR
                  TINYXML2_LIBRARY
                  TINYXML2_DEBUG_LIBRARY )


# display help message
IF( NOT TINYXML2_FOUND )
  # make FIND_PACKAGE friendly
  IF( NOT TinyXML2_FIND_QUIETLY )

    IF( TinyXML2_FIND_REQUIRED )
      SET( TINYXML2_ROOT_DIR "" CACHE PATH "TinyXML2 root dir")
      MESSAGE( FATAL_ERROR "TinyXML2 required but some headers or libs not found. Please specify it's location with TinyXML2_ROOT_DIR variable.")
    ELSE()
      MESSAGE(STATUS "ERROR: TinyXML2 was not found.")
    ENDIF()
  ENDIF()
else()
unset(TINYXML2_ROOT_DIR CACHE)
ENDIF()

