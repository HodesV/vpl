# CMake module that tries to find UMFPACK includes and libraries.
#
# The following variables are set if UMFPACK is found:
#   UMFPACK_FOUND         - True when the UMFPACK include directory is found.
#   UMFPACK_INCLUDE_DIR   - The directory where UMFPACK include files are.
#   UMFPACK_LIBRARIES     - List of all UMFPACK libraries.
#
# Usage:
#   In your CMakeLists.txt file do something like this:
#   ...
#   FIND_PACKAGE(Umfpack)
#   ...
#   INCLUDE_DIRECTORIES(${UMFPACK_INCLUDE_DIR})
#   ...
#   TARGET_LINK_LIBRARIES( mytarget ${UMFPACK_LIBRARIES} )

#if( UMFPACK_INCLUDE_DIR AND UMFPACK_LIBRARIES )
#  set( UMFPACK_FIND_QUIETLY TRUE )
#endif( UMFPACK_INCLUDE_DIR AND UMFPACK_LIBRARIES )

# Predefined search directories
if( WIN32 )
  set( UMFPACK_INC_SEARCHPATH
       "${UMFPACK_ROOT_DIR}"
       "$ENV{UMFPACK_ROOT_DIR}"
       "${INCLUDE_INSTALL_DIR}"
       "$ENV{ProgramFiles}/umfpack"
       C:/umfpack
       D:/umfpack
       )
  set( UMFPACK_INC_SEARCHSUFFIXES
       include
       )
else( WIN32 )
  set( UMFPACK_INC_SEARCHPATH
       "${UMFPACK_ROOT_DIR}"
       "$ENV{UMFPACK_ROOT_DIR}"
       "${INCLUDE_INSTALL_DIR}"
       /sw/include
       /usr/include
       /usr/local/include
       /opt/include
       /opt/local/include
       )
  set( UMFPACK_INC_SEARCHSUFFIXES
       umfpack
       umfpack/include
       suitesparse
       )
endif( WIN32 )


if( CMAKE_Fortran_COMPILER_WORKS )

#  if( NOT WIN32 )
    find_package( BLAS )
#  endif( NOT WIN32 )

  if( BLAS_FOUND )

  find_path( UMFPACK_INCLUDE_DIR
             NAMES
             umfpack.h
             PATHS
             ${UMFPACK_INC_SEARCHPATH}
             PATH_SUFFIXES
             ${UMFPACK_INC_SEARCHSUFFIXES}
             )

  get_filename_component( UMFPACK_INCLUDE_DIR_PATH ${UMFPACK_INCLUDE_DIR} PATH )

  set( UMFPACK_LIB_SEARCHPATH
       ${UMFPACK_INCLUDE_DIR_PATH}/lib
       ${LIB_INSTALL_DIR}
       /usr/lib
       /usr/local/lib
       )

  find_library( UMFPACK_LIBRARIES umfpack PATHS ${UMFPACK_LIB_SEARCHPATH} )

  if( UMFPACK_LIBRARIES )  
    get_filename_component( UMFPACK_LIB_DIR ${UMFPACK_LIBRARIES} PATH )
    
    find_library( AMD_LIBRARY amd PATHS ${UMFPACK_LIB_DIR} ${LIB_INSTALL_DIR} )
    if( AMD_LIBRARY )
      set( UMFPACK_LIBRARIES ${UMFPACK_LIBRARIES} ${AMD_LIBRARY} )
    endif( AMD_LIBRARY )

    find_library( COLAMD_LIBRARY colamd PATHS ${UMFPACK_LIB_DIR} ${LIB_INSTALL_DIR} )
    if( COLAMD_LIBRARY )
      set( UMFPACK_LIBRARIES ${UMFPACK_LIBRARIES} ${COLAMD_LIBRARY} )
    endif( COLAMD_LIBRARY )
  endif( UMFPACK_LIBRARIES )

  if( UMFPACK_LIBRARIES AND BLAS_LIBRARIES )
    set( UMFPACK_LIBRARIES ${UMFPACK_LIBRARIES} ${BLAS_LIBRARIES} )
  endif( UMFPACK_LIBRARIES AND BLAS_LIBRARIES )

  endif( BLAS_FOUND )

endif( CMAKE_Fortran_COMPILER_WORKS )

#include(FindPackageHandleStandardArgs)
#find_package_handle_standard_args(UMFPACK DEFAULT_MSG
#                                  UMFPACK_INCLUDE_DIR UMFPACK_LIBRARIES)
#
#mark_as_advanced(UMFPACK_INCLUDE_DIR UMFPACK_LIBRARIES AMD_LIBRARY COLAMD_LIBRARY)


# display help message
if( NOT UMFPACK_FOUND )
  # make FIND_PACKAGE friendly
  if( NOT UMFPACK_FIND_QUIETLY )
    if( UMFPACK_FIND_REQUIRED )
      message( FATAL_ERROR "UMFPACK required but some headers or libs not found. Please specify it's location with UMFPACK_ROOT_DIR variable.")
      set( UMFPACK_ROOT_DIR "" CACHE PATH "UMFPACK root dir" )
    else( UMFPACK_FIND_REQUIRED )
      message(STATUS "ERROR: UMFPACK was not found.")
    endif( UMFPACK_FIND_REQUIRED )
  endif( NOT UMFPACK_FIND_QUIETLY )
endif( NOT UMFPACK_FOUND )
