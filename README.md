    **********************************************************************
    * This file comes from MDSTk software and was modified for
    * 
    * VPL - Voxel Processing Library
    * Changes are Copyright 2014 3Dim Laboratory s.r.o.
    * All rights reserved.
    * 
    * Use of this file is governed by a BSD-style license that can be
    * found in the LICENSE file.
    * 
    * The original MDSTk legal notice can be found below.
    *
    * Medical Data Segmentation Toolkit (MDSTk)
    * Copyright (c) 2003-2012 by Michal Spanel, FIT, Brno University of Technology
    *
    **********************************************************************

Table of Contents

1. What is VPL?
2. Basic Features
3. Brief Guide
4. Installation
5. Source Code Documentation
6. Bug Reporting


1 What is VPL?
==============

VPL (Voxel Processing Library) is an open source collection of 2D/3D image 
processing tools originally aimed at medical images. It contains number of 
routines for volumetric data processing like 3D filtering, edge detection, 
segmentation, etc.

VPL is a successor of [MDSTk](http://sourceforge.net/projects/mdstk) (Medical 
Data Segmentation Toolkit) initiated by [3Dim Laboratory 
s.r.o.](http://www.3dim-laboratory.cz/) to further develop the toolkit and 
share it with wide open source community.

We will be pleased if somebody find out that this toolkit is useful.


---

2 Basic Features
================

- Multiplatform toolkit running on Linux, Windows and Mac OS X platforms.
  Supported compilers are
    GCC 4.6 and newer,
    MSVC (Visual Studio) 2008 and newer on Windows systems,
    LLVM/CLang++ 2.8 and newer. 
- The toolkit benefits from templated, fast and scalable C++ code.
- Simple modular architecture.
- Inter-module communication over channels implemented via files,
  named/unnamed pipes and shared memory.
- 2D/3D image processing library including
    convolution filters,
    edge detectors (e.g. 3D canny edge detector),
    FFT,
    segmentation algorithms (thresholding, RG, Watersheds, etc.),
    texture analysis (LBP features).
- Experimental support for the OpenCV library and OpenCV images.
  You can benefit from both the libraries.
- Multiplatform system library encapsulating threads, mutexes, conditions,
  critical sections, timers, etc.
- Math library based on Eigen and UMFPACK libraries providing
  static/dynamic vectors and matrices, sparse matrices, etc.
  including linear algebra routines (e.g. eigenvalues and eigenvectors
  problem).
- Several advanced algorithms implemented:
    Fuzzy c-Means clustering,
    optimization of GMMs (Gaussian Mixture Models) using the EM algorithm.
- Several image/slice formats supported: DICOM, JPEG and PNG.
- Public BSD-like license, all source codes available.


---

3 Brief Guide
=============

VPL was designed to be highly modular. It consists of a number
of separate modules providing fundamental functions and algorithms. Individual
modules may be connected using simple channels into more complex units.
Actually, there are channels implemented over files, named pipes, stdio and
shared memory. Typicaly, modules are chained using shell unnamed pipes
and the '|' character.


3.1 Running modules from Command Line
-------------------------------------

    -i  Specification of an input module channel. The channel is described by
    a string of the form 'medium[:attrib1[:attrib2[...]]]'. Allowed medium
    types are 'stdio', 'file', 'pipe' and 'shm'. First one has no attributes,
    file and pipe have only one filename attribute. Communication over the
    shared memory is not elementary, please see documentation for more details.
    
    -o  Output channel specification whose format is same as above.
    
    -log  Option selects a module log type. Allowed values are 'null' for no
    logging, 'stderr' for logging to the stderr, 'file' means logging to a file
    and 'both' selects logging to the both stderr and file.
    
    -h  Shows more detailed help.


3.2 Samples
-----------

    > vplLoadJPEG <data/jpeg/berounka.jpg | vplSliceInfo
    or
    > vplLoadJPEG -i file:data/jpeg/berounka.jpg | vplSliceInfo
    - Reads input image and prints information about it.

    > vplLoadDicom <data/dicom/80.dcm | vplSliceRange -auto | vplSliceView
    - Loads DICOM image data, changes pixel value range from 12-bit
      (medical density data) to the range 0..255 (8-bit grayscale
      image) and shows the final grayscale image
      in a window.

    > vplLoadDicom <data/dicom/80.dcm \
        | vplSliceFilter -filter gauss -sigma 1.0 \
        | vplSliceRange -auto | vplSliceView
    - Gaussian smoothing of the input image.


3.3 Basic Modules
-----------------

Source codes of VPL modules can be found in 'src/modules' directory.

Module Name              | Description
------------------------ | -----------------------------------------------------
vplCreateSHM             | Creates and handles a new block of shared memory.
vplLoadDicom             | Reads image in DICOM format from an input channel and converts it to vplTk native format. Typical output is a density 12-bit grayscale image called slice in medical imaging.
vplLoadJPEG              | Reads and converts JPEG image.
vplLoadPNG               | Reads and converts PNG image.
vplMakeVolume            | Module makes density volume data (grayscale 3D image) from several input slices.
vplRGBImage2Slice        | Converts input RGB image to slice.
vplRGBImageView          | Shows input RGB image in OpenGL window.  
vplSaveJPEG              | Converts input slice to the JPEG image format.
vplSavePNG               | Converts input slice to the PNG image format.

Module Name              | Description
------------------------ | -----------------------------------------------------
vplSliceAffineTransform  | Affine geometric image transform.
vplSliceCornerDetector   | Provides basic image corner detection (e.g. Harris)
vplSliceCut              | Keeps values of a specified interval of pixels unchanged, remaining ones are set to zero.
vplSliceEdgeDetector     | Advanced edge detection algorithms (e.g. Canny edge detector).
vplSliceFFT              | This sample module computes two-dimensional DFT of input slice.
vplSliceFilter           | Provides several image filtering functions such as Median, Sobel and Prewit operators, Gaussian smoothing and many others.
vplSliceHistEqualization | Histogram equalization.
vplSliceHistogram        | Shows histogram of input slice.
vplSliceInfo             | Shows information about input slice.
vplLBPCompare            | Compares LBP histograms of two images.
vplLBPExtract            | Extracts LBP codes from an image.
vplLBPHistogram          | Estimates normalized LBP histogram of an image.
vplSliceOpticalFlow      | Estimates optical flow between two images using Lucas-Kanade method.
vplSliceRange            | Extends a given pixel value interval to specified range (linear interpolation).
vplSliceSeg              | Sample slice segmentation module.
vplSliceSegEM            | Pixel-based segmentation using GMMs (Gaussian Mixture Models and well known Expectation-Maximization algorithm.
vplSliceSegFCM           | Another pixel-based segmentation algorithm. It uses the fuzzy C-means clustering algorithm.
vplSliceSegHT            | Slice segmentation based on histogram thresholding.
vplSliceSegRG            | Simple region growing segmentation algorithm.
vplSliceSegWatersheds    | Watersheds-based image segmentation.
vplSliceSubsampling      | Module demonstrates use of the Gaussian pyramid for image subsampling.
vplSliceTresholding      | Simple pixel value tresholding.
vplSliceView             | Draws input slice using GLUT and OpenGL.

Module Name              | Description
------------------------ | -----------------------------------------------------
vplVolumeBlending        | Merges two volumes.
vplVolumeCut             | Leaves a specified interval of voxel values unchanged and sets remaining ones are set to zero.
vplVolumeEdgeDetector    | 3D Canny edge detection algorithm.
vplVolumeFilter          | Several volume filtering functions (Median, Sobel operator, etc.)
vplVolumeHistEqualization | Volume data histogram equalization.
vplVolumeHistogram       | Shows volume data histogram.
vplVolumeInfo            | Prints information about input volume.
vplVolumeLandmarkDetector | Experimental algorithms for detection of landmark points in volume data.
vplVolumeRange           | Extends a given voxel value interval to specified range.
vplVolumeSeg             | Sample volume segmentation module.
vplVolumeSegHT           | Volume segmentation based on histogram thresholding.
vplVolumeSegRG           | Region growing segmentation of volume data. Very slow implementation! Should be remade in future.
vplVolumeSlit            | Splits input volume into several slices.
vplVolumeTresholding     | Simple voxel value tresholding.
vplVolumeView            | Reads input volume and visualizes it using GLUT and OpenGL.


---

4 Installation
==============

4.1 Required Software
---------------------

You should have the following software and libraries installed to compile and use VPL on all the platforms:

- CMake (Cross-platform Make) - open-source build system.
  "CMake is used to control the software compilation process using simple
  platform and compiler independent configuration files. CMake generates
  native makefiles and workspaces that can be used in the compiler
  environment of your choice."
  http://www.cmake.org/

- OpenGL and GLUT/Freeglut graphics libraries.

### Windows platform (MinGW)
- MinGW (Minimalist GNU for Windows) together with the GCC compiler.
  http://www.mingw.org/

- MSYS (MinGW - Minimal SYStem)
  http://www.mingw.org/msys.shtml

- Windows version of the GLUT/Freeglut library

  Precompiled Freeglut library for the MinGW compiler can be downloaded
  from the MDSTk web page together with all required 3rd party libraries.

- WinAPI header files (can be downloaded from the MinGW web site).

### Windows platform (MSVC):
- Windows version of the GLUT/Freeglut library for MSVC

  Prebuilt Freeglut library for the Visual Studio can be downloaded from
  the MDSTk web page together with all required 3rd party libraries.

### Linux platform:
- Both OpenGL and GLUT library should be alredy installed in all standard
  Linux distributions.
- Check that 32-bit versions of these libraries have been found!
  Otherwise, modify CMake variables MDSTk_OPENGL and MDSTk_GLUT manually.

- All other required libraries can be downloaded separately.
  Alternatively, you can use later JPEG, ZLIB, PNG, UMFPACK and FFTW
  libraries pre-installed on your system.
        
### Mac OS X platform:
- tbd


4.2 Compilation using CMake
---------------------------

Download latest version of the MDSTk from its web site and unpack it somewhere.
If you don't have CMake installed on your system let's do it now. 

Where appropriate, download the package of prebuilt required 3rd party libraries
and unpack it into the '3rdParty' directory.

Under Linux use the 'cmake' or 'ccmake' utilities from a build directory
different from the source directory. Advantage of such out-of-source build
is that temporary files created by CMake and compiler won't disturb the source
directory. Also, it makes possible to have multiple independent build targets
by creating multiple build directories.

    > cd VPL
    > mkdir build
    > cd build
    > ccmake ..
    > make

All executables will be placed in the 'build/bin' directory and libraries
in the 'build/lib' directory.

If you want to build debug versions of libraries and binaries, modify
the CMake variable 'CMAKE_BUILD_TYPE'

    > ccmake .. -DCMAKE_BUILD_TYPE=Debug

Under Windows use the utility 'CMakeSetup' (or 'cmake-gui' recently) to generate
project and solution files for your MS Visual Studio. Open the solution
'MDSTk.sln' which will be placed in the build directory and compile all
libraries first. Afterwards, choose and compile required modules
(optionally TESTs and documentation).

Alternatively, you can use the same utility to generate Makefiles for MinGW
and MSYS. In the next step, the 'make' utility must be called from the build
directory as mentioned above.


4.3 Eclipse Project
-------------------

Eclipse does not support projects which have linked resources pointing
to a parent directory. So we recommend to create your build directory
not as children, but as a sibling to the VPL source directory. E.g.:

    /path/to/VPL
    /path/to/build

You can generate Eclipse project using CMake by running the following command
from the build directory:

    > ccmake ../VPL -G"Eclipse CDT4 - Unix Makefiles"

Now you can import the generated project into your Eclipse.

    File->Import->General->Existing Projects into Workspace


4.4 Mac OS X
-------------------

You can generate Xcode project using CMake. Run the following command
from the build directory:

    > ccmake ../VPL -G"Xcode"


4.5 Cygwin
----------

You can compile VPL on Cygwin with minimal effort! You have to do
two things:

1) Download the latest version of CMake for Cygwin from
http://public.kitware.com/pub/cmake/cygwin/ and install it.

2) Follow the Linux compilation steps described in the VPL guide (create
directory for an out-of-source build, run the ccmake configuration utility,
run make, etc.).

There is only step you must do differently. During
the CMake configuration, enable the 'Advanced mode' and set the CMake
variable:
    CMAKE_CXX_FLAGS = '-mno-cygwin'


4.6 3rd Party Libraries
-----------------------

Here is a list of all external 3rd party libraries used by VPL.
For convenience, package of prebuilt libraries can be downloaded from the VPL
web site.

- Eigen library for linear algebra: vectors, matrices, and related algorithms
  by Benoit Jacob and Gael Guennebaud, et al.
  http://eigen.tuxfamily.org/
  
- FFTW (Fast Fourier Transform in the West) library by Matteo Frigo
  and Steven G. Johnson 
  http://www.fftw.org/
  
- UMFPACK library by Timothy A. Davis
  http://www.cise.ufl.edu/research/sparse/umfpack/  
  
- JPEG library by Independent JPEG Group
  http://www.ijg.org/
  
- libpng library by Glenn Randers-Pehrson et al.
  http://www.libpng.org/pub/png/
  
- zlib library by JEan-loup Gailly and Mark Adler
  http://www.zlib.net/
  
- Freglut library - The Free OpenGL Utility Toolkit
  http://freeglut.sourceforge.net/ 

Some of these libraries are distributed under more restrictive license
then the toolkit one. For example the FFTW library uses the GNU GPL license.
If you feel restricted with this license, you can disable the library
during the CMake configuration step.

### Obtaining 3rd part libraries on Linux

     sudo apt-get install fftw3-dev
     sudo apt-get install zlib1g-dev
     sudo apt-get install libpng-dev
     sudo apt-get install libjpeg-dev
     sudo apt-get install libeigen3-dev
     sudo apt-get install libtinyxml2-dev
     sudo apt-get install libgtest-dev
     sudo apt-get install rapidjson-dev

For opengl modules:

     sudo apt-get install freeglut3-dev

---

5 Source Code
=============

All source codes of VPL libraries and modules can be found in the 'src'
directory. Because of the templated design, huge code part can be found
in header files which reside in the 'include' directory.


5.1 Documentation
-----------------

VPL uses Doxygen as a documentation system. Doxygen makes it easy for
a programmer to provide up-to-date source code documentation because
it is a part of the source code. This documentation can be a great help
for you if you want to find or add some toolkit feature. Automatically
generated documentation is placed in the 'doc/doxygen' directory.
    
How can you build the documentation? First, install the Doxygen system
(http://www.doxygen.org/) and optionaly the Graphviz package
(http://graphviz.org/) for drawing graphs such as class diagram.
    
If you use Makefiles and 'make'
command to compile the toolkit just type
    
    > make doc
    
Even easier it is under MS Visual Studio. Open the generated solution
choose project 'ALL_DOC' and compile it. This will make Doxygen
documentation for you. You will find it in your build directory.  


---

6 Bug Reporting
===============

We don't promise that this software is completely bug free. If you encounter
any issue, please let us now.

- Report the bug using 
  [Bitbucket's issue tracker](https://bitbucket.org/3dimlab/vpl/issues),
- mail your bug reports to spanel(at)3dim-laboratory.cz,

or

- fork the repository, fix it, and send us a pull request.
